# PeinturePROJECT

PeintureProject c'est un site qui presente des peinture 

# Environnement de developement 

### Pré requis 
* PHP 7.4.26
* composer
* Symfony CLI
* Docker
* Docker-compose

### Lancer l'environnement de dev
'''bash
symfony server:start
docker-compose up

### Lancer des tests Unitaire 
'''bash
php bin/phpunit --testdox


