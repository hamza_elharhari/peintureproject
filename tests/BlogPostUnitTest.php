<?php

namespace App\Tests;

use App\Entity\Blogpost;
use PHPUnit\Framework\TestCase;
use DateTime;

class BlogPostUnitTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }
    public function testIsTrue()
    {
        
        $blogPost = new Blogpost();
        $dateTime = new DateTime();
        
        $blogPost->setTitre('titre')
        ->setSlug('slug')
        ->setContenu('contenu')
        ->setCreatedAt($dateTime);
        

        $this->assertTrue($blogPost->getTitre()=== 'titre');
        $this->assertTrue($blogPost->getSlug()=== 'slug' );
        $this->assertTrue($blogPost->getContenu()=== 'contenu' );
        $this->assertTrue($blogPost->getCreatedAt()=== $dateTime );
        
    }

    public function testIsFalse()
    {
        $blogPost = new Blogpost();
        $dateTime = new DateTime();
        
        $blogPost->setTitre('titre')
        ->setSlug('slug')
        ->setContenu('contenu')
        ->setCreatedAt($dateTime);
        

        $this->assertFalse($blogPost->getTitre()=== 'titre1');
        $this->assertFalse($blogPost->getSlug()=== 'slug1' );
        $this->assertFalse($blogPost->getContenu()=== 'test' );
        $this->assertFalse($blogPost->getCreatedAt()=== new DateTime() );
    }

    public function testIsEmpty(){
        $blogPost = new Blogpost();

        $this->assertEmpty($blogPost->getTitre());
        $this->assertEmpty($blogPost->getSlug());
        $this->assertEmpty($blogPost->getContenu());
        $this->assertEmpty($blogPost->getCreatedAt());
        
    }

}
