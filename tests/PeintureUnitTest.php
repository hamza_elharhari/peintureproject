<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\User;
use App\Entity\Peinture;
use DateTime;
use PHPUnit\Framework\TestCase;

class PeintureUnitTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }
    public function testIsTrue()
    {
        $user = new User();
        $peinture = new Peinture();
        $categorie = new Categorie();
        $dateTime = new DateTime();
        
        $peinture->setEnVente(true)
        ->setHauteur('20.20')
        ->setLargeur('20.20')
        ->setNom('el harhari')
        ->setCreatedAt($dateTime)
        ->setPortfolio(true)
        ->setSlg('slug')
        ->setFile('file')
        ->setDateRealisation($dateTime)
        ->setPrix('20.20')
        ->addCategorie($categorie)
        ->setUser($user);

        $this->assertTrue($peinture->isEnVente()=== true);
        $this->assertTrue($peinture->getLargeur()=== '20.20' );
        $this->assertTrue($peinture->getHauteur()=== '20.20' );
        $this->assertTrue($peinture->getCreatedAt()=== $dateTime );
        $this->assertTrue($peinture->getNom()=== "el harhari" );
        $this->assertTrue($peinture->getSlg()=== "slug" );
        $this->assertTrue($peinture->getPrix()=== '20.20');
        $this->assertTrue($peinture->getDateRealisation()=== $dateTime );
        $this->assertTrue($peinture->getFile()=== "file" );
        $this->assertTrue($peinture->isPortfolio()=== true );
        $this->assertContains($categorie , $peinture->getCategorie() );
        $this->assertTrue($peinture->getUser()=== $user );
    }

    public function testIsFalse()
    {
        $user = new User();
        $peinture = new Peinture();
        $categorie = new Categorie();
        $dateTime = new DateTime();
        
        $peinture->setEnVente(true)
        ->setHauteur('20.20')
        ->setLargeur('20.20')
        ->setNom('el harhari')
        ->setCreatedAt($dateTime)
        ->setPortfolio(true)
        ->setSlg('slug')
        ->setFile('file')
        ->setDateRealisation($dateTime)
        ->setPrix('20.20')
        ->addCategorie($categorie)
        ->setUser($user);

        $this->assertFalse($peinture->isEnVente()=== false);
        $this->assertFalse($peinture->getLargeur()=== 22.20 );
        $this->assertFalse($peinture->getHauteur()=== 22.20  );
        $this->assertFalse($peinture->getCreatedAt()=== new DateTime() );
        $this->assertFalse($peinture->getNom()=== "el harhariA" );
        $this->assertFalse($peinture->getSlg()=== "slog" );
        $this->assertFalse($peinture->getPrix()=== 22.20);
        $this->assertFalse($peinture->getDateRealisation()=== new DateTime() );
        $this->assertFalse($peinture->getFile()=== "fileF" );
        $this->assertFalse($peinture->isPortfolio()=== false);
        $this->assertNotContains(new Categorie() , $peinture->getCategorie() );
        $this->assertFalse($peinture->getUser()=== new User() );
    }

    public function testIsEmpty(){
        $peinture = new Peinture();

        $this->assertEmpty($peinture->isEnVente());
        $this->assertEmpty($peinture->getLargeur());
        $this->assertEmpty($peinture->getHauteur());
        $this->assertEmpty($peinture->getCreatedAt());
        $this->assertEmpty($peinture->getNom());
        $this->assertEmpty($peinture->getSlg());
        $this->assertEmpty($peinture->getPrix());
        $this->assertEmpty($peinture->getDateRealisation() );
        $this->assertEmpty($peinture->getFile() );
        $this->assertEmpty($peinture->isPortfolio());
        $this->assertEmpty($peinture->getCategorie() );
        $this->assertEmpty($peinture->getUser() );
    }

}
