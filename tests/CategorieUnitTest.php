<?php

namespace App\Tests;

use App\Entity\Categorie;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }
    public function testIsTrue()
    {
        $categorie = new Categorie();
        $categorie->setNom("hamza")
        ->setDescription("description")
        ->setSlug('slug');

        $this->assertTrue($categorie->getNom()=== "hamza" );
        $this->assertTrue($categorie->getDescription()=== "description" );
        $this->assertTrue($categorie->getSlug()=== "slug" );
       
    }

    public function testIsFalse()
    {
        $categorie = new Categorie();
        $categorie->setNom("hamza")
        ->setDescription("description")
        ->setSlug('slug');

        $this->assertFalse($categorie->getNom()=== "toto" );
        $this->assertFalse($categorie->getDescription()=== "kok" );
        $this->assertFalse($categorie->getSlug()=== "bug" );
    }

    public function testIsEmpty(){
        $categorie = new Categorie();

        $this->assertEmpty($categorie->getNom());
        $this->assertEmpty($categorie->getDescription() );
        $this->assertEmpty($categorie->getSlug() );
    }
}
