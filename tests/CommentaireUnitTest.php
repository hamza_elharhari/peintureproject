<?php

namespace App\Tests;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }
    public function testIsTrue()
    {
        $blogPost = new Blogpost();
        $peinture = new Peinture();
        $dateTime = new DateTime();
        $commentaire = new Commentaire();
        
        $commentaire->setAuteur('auteur')
        ->setEmail('test@tets.com')
        ->setContenu('contenu')
        ->setCreatedAt($dateTime)
        ->setBlogpost($blogPost)
        ->setPeinture($peinture);


        $this->assertTrue($commentaire->getAuteur()=== 'auteur');
        $this->assertTrue($commentaire->getEmail()=== 'test@tets.com' );
        $this->assertTrue($commentaire->getContenu()=== 'contenu' );
        $this->assertTrue($commentaire->getCreatedAt()=== $dateTime );
        $this->assertTrue($commentaire->getPeinture()=== $peinture );
        $this->assertTrue($commentaire->getBlogpost()=== $blogPost );

    }

    public function testIsFalse()
    {
        $blogPost = new Blogpost();
        $peinture = new Peinture();
        $dateTime = new DateTime();
        $commentaire = new Commentaire();
        
        $commentaire->setAuteur('auteur')
        ->setEmail('test@tets.com')
        ->setContenu('contenu')
        ->setCreatedAt($dateTime)
        ->setBlogpost(new Blogpost())
        ->setPeinture(new Peinture());


        $this->assertFalse($commentaire->getAuteur()=== 'auteur9');
        $this->assertFalse($commentaire->getEmail()=== '9test@tets.com' );
        $this->assertFalse($commentaire->getContenu()=== '9contenu' );
        $this->assertFalse($commentaire->getCreatedAt()=== new DateTime());
        $this->assertFalse($commentaire->getPeinture()=== $peinture );
        $this->assertFalse($commentaire->getBlogpost()=== $blogPost );
    }

    public function testIsEmpty(){

        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getBlogpost());
       
    }
}
