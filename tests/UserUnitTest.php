<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }

    public function testIsTrue()
    {
        $user = new User();
        $user->setEmail("hamza@test.com")
        ->setAPropos("apropos de propos")
        ->setPrenom('hamza')
        ->setNom('el harhari')
        ->setPassword("password")
        ->setInstagram("instagram");

        $this->assertTrue($user->getEmail()=== "hamza@test.com" );
        $this->assertTrue($user->getAPropos()=== "apropos de propos" );
        $this->assertTrue($user->getPrenom()=== "hamza" );
        $this->assertTrue($user->getPassword()=== "password" );
        $this->assertTrue($user->getNom()=== "el harhari" );
        $this->assertTrue($user->getInstagram()=== "instagram" );
    }

    public function testIsFalse()
    {
        $user = new User();
        $user->setEmail("hamza@test.com")
        ->setAPropos("apropos de propos")
        ->setPrenom('hamza')
        ->setNom('el harhari')
        ->setPassword("password")
        ->setInstagram("instagram");

        $this->assertFalse($user->getEmail()=== "toto@test.com" );
        $this->assertFalse($user->getAPropos()=== "false" );
        $this->assertFalse($user->getPrenom()=== "toto" );
        $this->assertFalse($user->getPassword()=== "mot de passe" );
        $this->assertFalse($user->getNom()=== "skoma" );
        $this->assertFalse($user->getInstagram()=== "facebook" );
    }

    public function testIsEmpty(){
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getInstagram());
    }
}
